import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TableaudeBord extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          alignment: Alignment.topLeft,
          child: Text(
            "Présentation                                                                                                             "
            "Terrarium Application est une application mobile de gestion de terrarium conçue pour le MyDil 2021 de EPSI Montpellier                                                                            "
            "Données du Terrarium                                                                                                     "
            "Éclairage      20 lx                                                                                                     "
            "TEMPERATURE    30 °C                                                                                                     "
            "HUMIDITE       0.5 %",
            style: TextStyle(
              color: Colors.black,
            ),
          )),
    );
  }
}

class Reglages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          alignment: Alignment.topLeft,
          child: Text(
            "Éclairage                                                                                                                                "
            "Intensité : reglable                                                                                              "
            "Luminosité : reglable                                                                                                     "
            "Humidité                                                                                                                                                "
            "Eau : reglable                                                                                                                            "
            "Température                                                                                                                      "
            "Chaleur : reglable",
            style: TextStyle(
              color: Colors.black,
            ),
          )),
    );
  }
}
